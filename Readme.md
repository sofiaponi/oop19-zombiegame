# Zombieversity

Zombieversity is a third person top-down shooter/survival. Your goal is to survive zombies hordes, as much as you can. The longer you stay alive the higher will be your score, challenge the community in the leaderboard and try to be the best.  

## Instruction

Execute the jar, be ready to play and start!


W,A,S,D and arrows keys to move the player, mouse to aim and shoot.

## Contributing
Gianluca De Bonis, Andrea De Crescenzo, Lorenzo M. Pagliazzi, Sofia Poni.

## License
Zombieversity & co. 