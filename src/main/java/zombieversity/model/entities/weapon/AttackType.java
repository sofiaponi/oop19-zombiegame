package zombieversity.model.entities.weapon;

/**
 * Used to distinguish long range attack and short range attacks.
 */
public enum AttackType {

    /**
     * A short range {@link Attack} that happens near the {@link zombieversity.model.entities.Player} generated by a {@link ShortRangeWeapon}.
     */
    SHORT_RANGE,

    /**
     * A long range {@link Attack} that travels far away from the {@link zombieversity.model.entities.Player} generated by a {@link LongRangeWeapon}.
     */
    LONG_RANGE 
}
