package zombieversity.model.entities.weapon;

/**
 * Represents every kind of meelee weapon like knifes, swords, bats...
 * It will be handled with standard methods in {@link Weapon}.
 */
public interface ShortRangeWeapon extends Weapon {

}
